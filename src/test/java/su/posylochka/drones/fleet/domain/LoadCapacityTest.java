package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import su.posylochka.drones.fleet.CustomAssertions;

public class LoadCapacityTest {

  @ParameterizedTest
  @ValueSource(ints = {-3, -2, -1, 0})
  public void canNotCreateCapacityWithTooLowValues(int value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new LoadCapacity(value),
        "limit must be > 0"
    );
  }

  @ParameterizedTest
  @ValueSource(ints = {501, 550, 1000})
  public void canNotCreateCapacityWithTooHighValues(int value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new LoadCapacity(value),
        "limit must be <= 500"
    );
  }

  @ParameterizedTest
  @ValueSource(ints = {1, 20, 50, 70, 90, 110, 130, 150, 200, 280, 300, 460, 500})
  public void canCreateLoadCapacityWithValidValues(int value) {
    var sut = new LoadCapacity(value);

    Assertions.assertEquals(value, sut.limit());
  }

}

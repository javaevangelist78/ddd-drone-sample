package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import su.posylochka.drones.fleet.CustomAssertions;

public class CodeTest {

  @ParameterizedTest
  @ValueSource(strings = {
      "a-12",
      "aaa",
      "  a",
      "a  ",
      "123-ds",
      "12-314",
      "-",
      "- -"
  })
  public void canNotCreateCodeWithIllegalValue(String code) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Code(code),
        "code can contain only uppercase letters, digits and _"
    );
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {
      "",
      " ",
      "  "
  })
  public void canNotCreateCodeWithBlankValue(String code) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Code(code),
        "code must be not blank"
    );
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "AZ_22",
      "12345",
      "99_OZF",
      "OZFF33"
  })
  public void canCreateCodeWithValidValues(String code) {
    var sut = new Code(code);

    Assertions.assertEquals(code, sut.value());
  }

}

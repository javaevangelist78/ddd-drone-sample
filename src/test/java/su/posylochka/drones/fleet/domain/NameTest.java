package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import su.posylochka.drones.fleet.CustomAssertions;

public class NameTest {

  @ParameterizedTest
  @ValueSource(strings = {
      "hello world",
      "this1 1s inv2lid",
      "zzzzzz$/*",
      "1234*9999"
  })
  public void canNotCreateNameWithIllegalValues(String value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Name(value),
        "name can contain only letters, digits, _ and -"
    );
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {"", " ", "  "})
  public void canNotCreateNameWithBlankValues(String value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Name(value),
        "name value must be not blank"
    );
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "aderal",
      "AdErAl",
      "Ad_eRAL",
      "AderaL_11-24",
      "phin_ome-al22",
      "Noshpa",
      "Phenotropil"
  })
  public void canCreateNameWithAllowedValues(String value) {
    var sut = new Name(value);

    Assertions.assertEquals(value, sut.value());
  }

}

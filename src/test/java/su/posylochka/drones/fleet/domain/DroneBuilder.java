package su.posylochka.drones.fleet.domain;

/**
 * @implNote Only for test purposes
 */
final class DroneBuilder {

  private SerialNumber serialNumber = new SerialNumber("AGENT-007");
  private Drone.Model model = Drone.Model.Heavyweight;
  private LoadCapacity capacity = new LoadCapacity(500);
  private Battery battery = new Battery(85);
  private Drone.State state = Drone.State.IDLE;

  static Drone justBuild() {
    return new DroneBuilder().build();
  }

  DroneBuilder withSerialNumber(SerialNumber serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  DroneBuilder withModel(Drone.Model model) {
    this.model = model;
    return this;
  }

  DroneBuilder withCapacity(LoadCapacity capacity) {
    this.capacity = capacity;
    return this;
  }

  DroneBuilder withState(Drone.State state) {
    this.state = state;
    return this;
  }

  DroneBuilder withBattery(Battery battery) {
    this.battery = battery;
    return this;
  }

  Drone build() {
    return new Drone(serialNumber, model, capacity, battery, state);
  }

}

package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import su.posylochka.drones.fleet.CustomAssertions;

import java.nio.file.Path;
import java.util.stream.Stream;

public class MedicationTest {

  @ParameterizedTest
  @MethodSource("medicationIllegalConstructorCases")
  public void canNotCreateMedicationWithIllegalArgs(Name name, int weightGram, Code code, Path imagePath, String message) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Medication(name, weightGram, code, imagePath),
        message
    );
  }

  static Stream<Arguments> medicationIllegalConstructorCases() {
    return Stream.of(
        Arguments.of(null, 100, new Code("AZ_22"), Path.of("/tmp"), "name must be not null"),
        Arguments.of(new Name("Phenotropil"), 0, new Code("PH_001"), Path.of("/tmp"), "weight must be > 0"),
        Arguments.of(new Name("Phenotropil"), -1, new Code("PH_001"), Path.of("/tmp"), "weight must be > 0"),
        Arguments.of(new Name("Phenotropil"), 100, null, Path.of("/tmp"), "code must be not null"),
        Arguments.of(new Name("Phenotropil"), 80, new Code("PH_001"), null, "image path must be not null")
    );
  }

}

package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import su.posylochka.drones.fleet.CustomAssertions;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BatteryTest {

  @ParameterizedTest
  @ValueSource(ints = {-1, 101, 120})
  public void canNotCreateDroneWithIllegalPercentage(int percentage) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Battery(percentage),
        "battery percentage should be in range from 0 to 100"
    );
  }

  @ParameterizedTest
  @MethodSource("enoughBatteryCharge")
  public void enoughChargeTrueWhenBatteryCharged(int percent) {
    var sut = new Battery(percent);

    Assertions.assertTrue(sut.isEnoughCharge());
  }

  @ParameterizedTest
  @MethodSource("lowBatteryCharge")
  public void enoughChargeFalseWhenBatteryLow(int percent) {
    var sut = new Battery(percent);

    Assertions.assertFalse(sut.isEnoughCharge());
  }

  private static Stream<Arguments> lowBatteryCharge() {
    return IntStream.rangeClosed(0, 24).mapToObj(Arguments::of);
  }

  private static Stream<Arguments> enoughBatteryCharge() {
    return IntStream.rangeClosed(25, 100).mapToObj(Arguments::of);
  }

}

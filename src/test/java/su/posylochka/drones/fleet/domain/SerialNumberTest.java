package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import su.posylochka.drones.fleet.CustomAssertions;

public class SerialNumberTest {

  @ParameterizedTest
  @ValueSource(strings = {
      "XQIHyjhmWC4b8UCTWQlwkn3XjDGm4vBKmMDBxQxXAS0pnmjgCMldOlkh4M0m2qVPSGONmVOz7VwnoXCnnOiFRnk6x963t6BH76ZrhuzzUwrgic"
  })
  public void canNotCreateInvalidSerialNumbers(String value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new SerialNumber(value),
        "max length of serial number is 100"
    );
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = {"", " ", "  "})
  public void canNotCreateBlankSerialNumbers(String value) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new SerialNumber(value),
        "serial number must be not blank"
    );
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "AJ-22 PZ43 KS22",
      "189-90 2000000 9516",
      "H",
      "HV5920434380",
      "900/72 9***33 19_AH-Z",
      "IQ7jCBTOYDLYXAUfvY7SiDPi4LuwDEseLw9BruiKTVz8RtA48NJyk97VbZ5GCLUZQctAufN9mTHR0F0wtQ0kEQxzaYRhe1ua7SGP"
  })
  public void canCreateValidSerialNumbers(String value) {
    var sut = new SerialNumber(value);

    Assertions.assertEquals(value, sut.value());
  }

}

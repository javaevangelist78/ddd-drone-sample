package su.posylochka.drones.fleet.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import su.posylochka.drones.fleet.CustomAssertions;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DroneTest {

  @ParameterizedTest
  @MethodSource("droneConstructorArgs")
  public void canNotCreateDroneWithoutRequiredArgs(SerialNumber number, Drone.Model model, LoadCapacity capacity, Battery battery, Drone.State state, String expectedMessage) {
    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> new Drone(number, model, capacity, battery, state),
        expectedMessage
    );
  }

  @ParameterizedTest
  @MethodSource("lowBatteryChargeValues")
  public void canNotLoadDroneWhenLowBatteryCharge(int batteryLevel) {
    var sut = new DroneBuilder().withBattery(new Battery(batteryLevel)).build();
    var cargo = createSimpleCargo();

    Assertions.assertThrows(
        DroneCanNotTransportCargoException.class,
        () -> sut.loading(cargo)
    );
  }

  @ParameterizedTest
  @MethodSource("allNotIdleStateValues")
  public void canNotLoadDroneWhenItIsNotIdle(Drone.State state) {
    var sut = new DroneBuilder().withState(state).build();
    var cargo = createSimpleCargo();

    Assertions.assertThrows(
        DroneCanNotTransportCargoException.class,
        () -> sut.loading(cargo)
    );
  }

  @Test
  public void canNotLoadDroneWhenCargoWeightGreaterThanDroneCapacity() {
    var sut = new DroneBuilder().withCapacity(new LoadCapacity(25)).build();
    var cargo = createSimpleCargo();

    Assertions.assertThrows(
        DroneCanNotTransportCargoException.class,
        () -> sut.loading(cargo)
    );
  }

  @Test
  public void canLoadDroneWhenAllConditionsSatisfied() {
    var sut = DroneBuilder.justBuild();
    var cargo = createSimpleCargo();

    sut.loading(cargo);

    Assertions.assertEquals(cargo, sut.cargo());
    Assertions.assertEquals(Drone.State.LOADING, sut.state());
  }

  @Test
  public void canNotLoadingCompleteWhenDroneIsNotLoading() {
    var sut = DroneBuilder.justBuild();

    Assertions.assertThrows(
        DroneNotLoadingException.class,
        sut::loadingComplete
    );
  }

  @Test
  public void canLoadingCompleteWhenDroneNowInLoadingState() {
    var sut = DroneBuilder.justBuild();
    sut.loading(createSimpleCargo());

    sut.loadingComplete();

    Assertions.assertEquals(Drone.State.LOADED, sut.state());
  }

  @Test
  public void canNotLoadDroneWithNullCargo() {
    var sut = DroneBuilder.justBuild();

    CustomAssertions.assertExceptionThrownWithMessage(
        IllegalArgumentException.class,
        () -> sut.loading(null),
        "cargo must be not null"
    );
  }

  private static Stream<Arguments> droneConstructorArgs() {
    return Stream.of(
        Arguments.of(null, Drone.Model.Heavyweight, new LoadCapacity(200), new Battery(60), Drone.State.IDLE, "serial number must be not null"),
        Arguments.of(new SerialNumber("ABJ-01"), null, new LoadCapacity(500), new Battery(12), Drone.State.IDLE, "model must be not null"),
        Arguments.of(new SerialNumber("ABS-33"), Drone.Model.Lightweight, null, new Battery(25), Drone.State.IDLE, "load capacity must be not null"),
        Arguments.of(new SerialNumber("SW-12"), Drone.Model.Cruiserweight, new LoadCapacity(200), null, Drone.State.IDLE, "battery must be not null"),
        Arguments.of(new SerialNumber("IJS-12"), Drone.Model.Middleweight, new LoadCapacity(100), new Battery(90), null, "state must be not null")
    );
  }

  private static Stream<Arguments> lowBatteryChargeValues() {
    return IntStream.rangeClosed(0, 24).mapToObj(Arguments::of);
  }

  private static Stream<Arguments> allNotIdleStateValues() {
    return Arrays.stream(Drone.State.values()).filter(state -> state != Drone.State.IDLE).map(Arguments::of);
  }

  private static Cargo createSimpleCargo() {
    return new Cargo(
        List.of(
            new Medication(new Name("Aderal"), 50, new Code("ADDRL"), Path.of("/tmp"))
        )
    );
  }

}

package su.posylochka.drones.fleet;

import su.posylochka.drones.fleet.domain.Battery;
import su.posylochka.drones.fleet.domain.Cargo;
import su.posylochka.drones.fleet.domain.Drone;
import su.posylochka.drones.fleet.domain.LoadCapacity;
import su.posylochka.drones.fleet.domain.SerialNumber;
import su.posylochka.drones.fleet.repository.DroneRepository;
import su.posylochka.drones.fleet.request.DroneRequest;

import java.util.Collection;
import java.util.UUID;

public class DroneService {

  private final DroneRepository repository;

  public DroneService(DroneRepository repository) {
    this.repository = repository;
  }

  public UUID register(DroneRequest droneRequest) {
    var drone = Drone.idle(
        new SerialNumber(
            droneRequest.serialCode()
        ),
        droneRequest.model(),
        new LoadCapacity(
            droneRequest.weightLimit()
        ),
        new Battery(
            droneRequest.batteryCapacity()
        )
    );

    return register(drone);
  }

  public UUID register(Drone drone) {
    return repository.save(drone).id();
  }

  public Collection<Drone> allIdle() {
    return repository.findAllIdle();
  }

  public Collection<Drone> all() {
    return repository.all();
  }

  public void loadWithCargo(UUID droneId, Cargo cargo) {
    repository.findById(droneId).loading(cargo);
  }

  public void completeLoading(UUID droneId) {
    repository.findById(droneId).loadingComplete();
  }

  public Cargo loadDroneCargo(UUID droneId) {
    return repository.findById(droneId).cargo();
  }

  public Battery loadBatteryStatusForDrone(UUID droneId) {
    return repository.findById(droneId).battery();
  }

}

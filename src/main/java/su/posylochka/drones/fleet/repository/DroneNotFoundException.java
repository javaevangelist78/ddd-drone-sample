package su.posylochka.drones.fleet.repository;

import java.util.UUID;

public final class DroneNotFoundException extends RuntimeException {

  private DroneNotFoundException(String message) {
    super(message);
  }

  public static DroneNotFoundException fromId(UUID id) {
    return new DroneNotFoundException("Drone with id '" + id + "' not found");
  }

}

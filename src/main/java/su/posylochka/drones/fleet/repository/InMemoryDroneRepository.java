package su.posylochka.drones.fleet.repository;

import su.posylochka.drones.fleet.domain.Drone;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class InMemoryDroneRepository implements DroneRepository {

  private final CopyOnWriteArrayList<Drone> drones = new CopyOnWriteArrayList<>();

  @Override
  public Drone save(Drone drone) {
    Objects.requireNonNull(drone);

    var id = drone.id();

    if (id == null) {
      drone.assignId(UUID.randomUUID());
    }

    drones.add(drone);

    return drone;
  }

  @Override
  public Drone findById(UUID id) {
    Objects.requireNonNull(id);

    return drones.stream()
        .filter(drone -> drone.isSame(id))
        .findFirst()
        .orElseThrow(() -> DroneNotFoundException.fromId(id));
  }

  @Override
  public Collection<Drone> findAllIdle() {
    return drones.stream()
        .filter(Drone::isAllowedForLoading)
        .toList();
  }

  @Override
  public Collection<Drone> all() {
    return new ArrayList<>(drones);
  }

}

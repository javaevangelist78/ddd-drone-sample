package su.posylochka.drones.fleet.repository;

import su.posylochka.drones.fleet.domain.Drone;

import java.util.Collection;
import java.util.UUID;

public interface DroneRepository {

  Drone save(Drone drone);

  Drone findById(UUID id);

  Collection<Drone> findAllIdle();

  Collection<Drone> all();

}

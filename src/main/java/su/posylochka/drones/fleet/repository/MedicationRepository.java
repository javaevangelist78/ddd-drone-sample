package su.posylochka.drones.fleet.repository;

import su.posylochka.drones.fleet.domain.Medication;

import java.util.Collection;

public interface MedicationRepository {

  Collection<Medication> findAll();

}

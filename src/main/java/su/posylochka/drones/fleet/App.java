package su.posylochka.drones.fleet;

import su.posylochka.drones.fleet.domain.Cargo;
import su.posylochka.drones.fleet.domain.Code;
import su.posylochka.drones.fleet.domain.Drone;
import su.posylochka.drones.fleet.domain.Medication;
import su.posylochka.drones.fleet.domain.Name;
import su.posylochka.drones.fleet.repository.InMemoryDroneRepository;
import su.posylochka.drones.fleet.request.DroneRequest;

import java.nio.file.Path;
import java.util.List;

public class App {

  public static void main(String[] args) {
    var droneRepo = new InMemoryDroneRepository();
    var droneService = new DroneService(droneRepo);

    var heavyDroneId = droneService.register(new DroneRequest("UMP-223", Drone.Model.Heavyweight, 500, 100));
    droneService.register(new DroneRequest("JKS-103", Drone.Model.Lightweight, 120, 27));

    droneService.allIdle().forEach(System.out::println);

    droneService.loadWithCargo(
        heavyDroneId,
        new Cargo(
            List.of(
                new Medication(new Name("Aderal"), 150, new Code("ADRL_222"), Path.of("/tmp")),
                new Medication(new Name("Noshpa"), 300, new Code("NOSHP_100"), Path.of("/tmp"))
            )
        )
    );

    droneService.allIdle().forEach(System.out::println);
    droneService.all().forEach(System.out::println);

    System.out.println(
        droneService.loadDroneCargo(heavyDroneId)
    );

    System.out.println(
        droneService.loadBatteryStatusForDrone(heavyDroneId)
    );
  }

}

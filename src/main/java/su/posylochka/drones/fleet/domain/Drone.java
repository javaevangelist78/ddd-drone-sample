package su.posylochka.drones.fleet.domain;

import java.util.Objects;
import java.util.UUID;

public final class Drone {

  private UUID id;
  private final SerialNumber serialNumber;
  private final Model model;
  private final LoadCapacity loadCapacity;
  private final Battery battery;

  private State state;
  private Cargo cargo;

  public Drone(SerialNumber serialNumber, Model model, LoadCapacity loadCapacity, Battery battery, State state) {
    Contract.notNull(serialNumber, "serial number must be not null");
    Contract.notNull(model, "model must be not null");
    Contract.notNull(loadCapacity, "load capacity must be not null");
    Contract.notNull(battery, "battery must be not null");
    Contract.notNull(state, "state must be not null");

    this.serialNumber = serialNumber;
    this.model = model;
    this.loadCapacity = loadCapacity;
    this.battery = battery;
    this.state = state;
  }

  public static Drone idle(SerialNumber serialNumber, Model model, LoadCapacity capacity, Battery battery) {
    return new Drone(serialNumber, model, capacity, battery, State.IDLE);
  }

  public UUID id() {
    return id;
  }

  public void assignId(UUID id) {
    this.id = id;
  }

  public boolean isSame(UUID id) {
    return this.id != null && this.id.equals(id);
  }

  public Model model() {
    return model;
  }

  public Cargo cargo() {
    return cargo;
  }

  public Battery battery() {
    return battery;
  }

  public State state() {
    return state;
  }

  public boolean isAllowedForLoading() {
    return isIdle() && battery.isEnoughCharge();
  }

  public void loading(Cargo cargo) {
    Contract.notNull(cargo, "cargo must be not null");

    var cargoWeight = cargo.totalWeight();

    if (!isAllowedForLoading() || loadCapacity.isLessThan(cargoWeight)) {
      throw new DroneCanNotTransportCargoException();
    }

    this.state = State.LOADING;
    this.cargo = cargo;
  }

  public void loadingComplete() {
    if (!isLoading()) {
      throw new DroneNotLoadingException();
    }
    this.state = State.LOADED;
  }

  private boolean isIdle() {
    return State.IDLE == state;
  }

  private boolean isLoading() {
    return State.LOADING == state;
  }

  @Override
  public String toString() {
    return "Drone{" +
        "id=" + id +
        ", serialNumber=" + serialNumber +
        ", model=" + model +
        ", loadCapacity=" + loadCapacity +
        ", battery=" + battery +
        ", state=" + state +
        ", cargo=" + cargo +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Drone drone = (Drone) o;
    return Objects.equals(id, drone.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  public enum Model {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
  }

  public enum State {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
  }

}

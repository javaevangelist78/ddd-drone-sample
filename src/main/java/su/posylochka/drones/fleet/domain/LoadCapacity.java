package su.posylochka.drones.fleet.domain;

public final class LoadCapacity {

  private static final int UPPER_LIMIT = 500;

  private final int limit;

  public LoadCapacity(int limit) {
    Contract.shouldBeTrue(limit > 0, "limit must be > 0");
    Contract.shouldBeTrue(limit <= UPPER_LIMIT, "limit must be <= " + UPPER_LIMIT);

    this.limit = limit;
  }

  public int limit() {
    return limit;
  }

  public boolean isLessThan(int gram) {
    return limit < gram;
  }

  @Override
  public String toString() {
    return limit() + "g.";
  }

}

package su.posylochka.drones.fleet.domain;

import java.util.Collection;
import java.util.Objects;

public class Cargo {

  private final Collection<Medication> medications;

  public Cargo(Collection<Medication> medications) {
    Contract.notEmpty(medications, "medications must be not null and not empty");

    this.medications = medications;
  }

  int totalWeight() {
    return medications.stream()
        .mapToInt(Medication::weightGram)
        .sum();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Cargo cargo = (Cargo) o;
    return medications.equals(cargo.medications);
  }

  @Override
  public int hashCode() {
    return Objects.hash(medications);
  }

  @Override
  public String toString() {
    return "Cargo{" +
        "medications=" + medications +
        ", totalWeight=" + totalWeight() + "g." +
        '}';
  }

}

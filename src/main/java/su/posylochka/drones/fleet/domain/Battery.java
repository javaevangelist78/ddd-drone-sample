package su.posylochka.drones.fleet.domain;

public final class Battery {

  private static final int LOW_CHARGE_PERCENT = 25;
  private static final int MAX_CHARGE_PERCENT = 100;

  private final int percentage;

  public Battery(int percentage) {
    Contract.shouldBeTrue(percentage >= 0 && percentage <= MAX_CHARGE_PERCENT,
        "battery percentage should be in range from 0 to 100");

    this.percentage = percentage;
  }

  public boolean isEnoughCharge() {
    return percentage >= LOW_CHARGE_PERCENT;
  }

  @Override
  public String toString() {
    return percentage + "%";
  }

}

package su.posylochka.drones.fleet.domain;

public final class SerialNumber {

  private final String value;

  public SerialNumber(String value) {
    Contract.hasText(value, "serial number must be not blank");
    Contract.shouldBeTrue(value.length() <= 100, "max length of serial number is 100");

    this.value = value;
  }

  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return value();
  }

}

package su.posylochka.drones.fleet.domain;

public final class Code {

  private static final String VALID_CODE_PATTERN = "^[A-Z0-9_]+$";

  private final String value;

  public Code(String value) {
    Contract.hasText(value, "code must be not blank");
    Contract.shouldBeTrue(value.matches(VALID_CODE_PATTERN), "code can contain only uppercase letters, digits and _");

    this.value = value;
  }

  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return value();
  }

}

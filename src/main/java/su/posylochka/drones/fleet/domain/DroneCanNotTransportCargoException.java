package su.posylochka.drones.fleet.domain;

public final class DroneCanNotTransportCargoException extends RuntimeException {

  public DroneCanNotTransportCargoException() {
    super("Drone can't deliver this cargo");
  }

}

package su.posylochka.drones.fleet.domain;

public final class Name {

  private static final String VALID_NAME_PATTERN = "^[\\w-]+$";

  private final String value;

  public Name(String value) {
    Contract.hasText(value, "name value must be not blank");
    Contract.shouldBeTrue(value.matches(VALID_NAME_PATTERN), "name can contain only letters, digits, _ and -");

    this.value = value;
  }

  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return value();
  }

}

package su.posylochka.drones.fleet.domain;

import java.util.Collection;

final class Contract {

  private Contract() {
  }

  static void notEmpty(Collection<?> items, String message) {
    shouldBeTrue(items != null && !items.isEmpty(), message);
  }

  static void notNull(Object value, String message) {
    shouldBeTrue(value != null, message);
  }

  static void hasText(String value, String message) {
    shouldBeTrue(null != value && !value.isBlank(), message);
  }

  static void shouldBeTrue(boolean result, String message) {
    if (!result) {
      throw new IllegalArgumentException(message);
    }
  }

}

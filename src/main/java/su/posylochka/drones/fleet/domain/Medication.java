package su.posylochka.drones.fleet.domain;

import java.nio.file.Path;

public final class Medication {

  private final Name name;
  private final int weightGram;
  private final Code code;
  private final Path imagePath;

  public Medication(Name name, int weightGram, Code code, Path imagePath) {
    Contract.notNull(name, "name must be not null");
    Contract.shouldBeTrue(weightGram > 0, "weight must be > 0");
    Contract.notNull(code, "code must be not null");
    Contract.notNull(imagePath, "image path must be not null");

    this.name = name;
    this.weightGram = weightGram;
    this.code = code;
    this.imagePath = imagePath;
  }

  public Name name() {
    return name;
  }

  public int weightGram() {
    return weightGram;
  }

  public Code code() {
    return code;
  }

  public Path imagePath() {
    return imagePath;
  }

  @Override
  public String toString() {
    return "Medication{" +
        "name=" + name +
        ", weight=" + weightGram + "g." +
        ", code=" + code +
        ", imagePath=" + imagePath +
        '}';
  }

}

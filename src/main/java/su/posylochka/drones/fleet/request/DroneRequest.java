package su.posylochka.drones.fleet.request;

import su.posylochka.drones.fleet.domain.Drone;

public record DroneRequest(String serialCode, Drone.Model model, int weightLimit, int batteryCapacity) {

}
